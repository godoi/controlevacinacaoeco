package web.controlevacinacao.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import web.controlevacinacao.ajax.NotificacaoAlertify;
import web.controlevacinacao.ajax.TipoNotificacaoAlertify;
import web.controlevacinacao.model.Status;
import web.controlevacinacao.model.Vacina;
import web.controlevacinacao.model.filter.VacinaFilter;
import web.controlevacinacao.pagination.PageWrapper;
import web.controlevacinacao.repository.VacinaRepository;
import web.controlevacinacao.service.VacinaService;

@Controller
@RequestMapping("/vacinas")
public class VacinaController {

	private static final Logger logger = LoggerFactory.getLogger(VacinaController.class);

	@Autowired
	private VacinaRepository vacinaRepository;

	@Autowired
	private VacinaService vacinaService;

	@GetMapping("/abrirpesquisar")
	public String abrirPesquisar() {
		return "vacina/pesquisar";
	}

	@GetMapping("/pesquisar")
	public String pesquisar(VacinaFilter filtro, Model model,
			@PageableDefault(size = 5) @SortDefault(sort = "codigo", direction = Sort.Direction.ASC) Pageable pageable,
			HttpServletRequest request) {
		Page<Vacina> pagina = vacinaRepository.pesquisar(filtro, pageable);
		PageWrapper<Vacina> paginaWrapper = new PageWrapper<>(pagina, request);
		model.addAttribute("pagina", paginaWrapper);
		return "vacina/mostrarvacinas";
	}

	@GetMapping("/cadastrar")
	public String abrirCadastrar(Vacina vacina) {
		return "vacina/cadastrar";
	}

	@PostMapping("/cadastrar")
	public String cadastrar(@Valid Vacina vacina, BindingResult resultado) {
		if (resultado.hasErrors()) {
			logger.info("A vacina recebida para cadastrar não é válida.");
			logger.info("Erros encontrados:");
			for (FieldError erro : resultado.getFieldErrors()) {
				logger.info("{}", erro);
			}
			return "vacina/cadastrar";
		} else {
			vacinaService.salvar(vacina);
			return "redirect:/vacinas/cadastrosucesso";
		}
	}

	@GetMapping("/cadastrosucesso")
	public String mostrarCadastroSucesso(Vacina vacina, Model model) {
		NotificacaoAlertify notificacao = 
				new NotificacaoAlertify("Cadastro de vacina efetuado com sucesso.",
						                TipoNotificacaoAlertify.SUCESSO);
		model.addAttribute("notificacao", notificacao);
		return "vacina/cadastrar";
	}

	@PostMapping("/abriralterar")
	public String abrirAlterar(Vacina vacina, String queryString, Model model) {
		model.addAttribute("queryString", queryString);
		return "vacina/alterar";
	}

	@PostMapping("/alterar")
	public String alterar(@Valid Vacina vacina, BindingResult resultado, 
			              String queryString, Model model,
			              RedirectAttributes atributos) {
		if (resultado.hasErrors()) {
			logger.info("A vacina recebida para alterar não é válida.");
			logger.info("Erros encontrados:");
			for (FieldError erro : resultado.getFieldErrors()) {
				logger.info("{}", erro);
			}
			model.addAttribute("queryString", queryString);
			return "vacina/alterar";
		} else {
			vacinaService.alterar(vacina);
			atributos.addFlashAttribute("queryString", queryString);
			return "redirect:/vacinas/alterarsucesso";
		}
	}

	@GetMapping("/alterarsucesso")
	public String mostrarAlterarSucesso(Model model) {
		NotificacaoAlertify notificacao = 
				new NotificacaoAlertify("Alteração de vacina efetuada com sucesso.",
						                TipoNotificacaoAlertify.SUCESSO);
		model.addAttribute("notificacao", notificacao);
		return "forward:/vacinas/pesquisar?" + model.getAttribute("queryString");
	}

	@PostMapping("/confirmarremocao")
	public String confirmarRemocao(Vacina vacina) {
		return "vacina/confirmarremocao";
	}

	@PostMapping("/remover")
	public String remover(Vacina vacina) {
		vacina.setStatus(Status.INATIVO);
		vacinaService.alterar(vacina);
		return "redirect:/vacinas/remocaosucesso";
	}

	@GetMapping("/remocaosucesso")
	public String mostrarRemocaoSucesso(Model model) {
		NotificacaoAlertify notificacao = 
				new NotificacaoAlertify("Remoção de vacina efetuada com sucesso.",
						                TipoNotificacaoAlertify.SUCESSO);
		model.addAttribute("notificacao", notificacao);
		return "vacina/pesquisar";
	}

}
