package web.controlevacinacao.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import web.controlevacinacao.model.Profissao;
import web.controlevacinacao.model.Status;
import web.controlevacinacao.model.filter.ProfissaoFilter;
import web.controlevacinacao.pagination.PageWrapper;
import web.controlevacinacao.repository.ProfissaoRepository;
import web.controlevacinacao.service.ProfissaoService;

@Controller
@RequestMapping("/profissoes")
public class ProfissaoController {

	private static final Logger logger = LoggerFactory.getLogger(ProfissaoController.class);

	@Autowired
	private ProfissaoRepository profissaoRepository;

	@Autowired
	private ProfissaoService profissaoService;

	@GetMapping("/abrirpesquisar")
	public String abrirPesquisa() {
		return "profissao/pesquisar";
	}

	@GetMapping("/pesquisar")
	public String pesquisar(ProfissaoFilter filtro, Model model,
			@PageableDefault(size = 10) @SortDefault(sort = "codigo", direction = Sort.Direction.ASC) Pageable pageable,
			HttpServletRequest request) {

		Page<Profissao> pagina = profissaoRepository.pesquisar(filtro, pageable);
		PageWrapper<Profissao> paginaWrapper = new PageWrapper<>(pagina, request);
		model.addAttribute("pagina", paginaWrapper);
		return "profissao/mostrartodas";
	}

	@GetMapping("/cadastrar")
	public String abrirCadastro(Profissao profissao) {
		return "profissao/cadastrar";
	}

	@PostMapping("/cadastrar")
	public String cadastrar(@Valid Profissao profissao, BindingResult resultado) {
		if (resultado.hasErrors()) {
			logger.info("A profissão recebida para cadastrar não é válida.");
			logger.info("Erros encontrados:");
			for (FieldError erro : resultado.getFieldErrors()) {
				logger.info("{}", erro);
			}
			return "profissao/cadastrar";
		} else {
			profissaoService.salvar(profissao);
			return "redirect:/profissoes/cadastro/sucesso";
		}
	}

	@GetMapping("/cadastro/sucesso")
	public String mostrarMensagemCadastroSucesso(Model model) {
		model.addAttribute("mensagem", "Cadastro de Profissão efetuado com sucesso.");
		return "mostrarmensagem";
	}

	@PostMapping("/abriralterar")
	public String abrirAlterar(Profissao profissao) {
		return "profissao/alterar";
	}

	@PostMapping("/alterar")
	public String alterar(@Valid Profissao profissao, BindingResult resultado) {
		if (resultado.hasErrors()) {
			logger.info("A profissão recebida para alterar não é válida.");
			logger.info("Erros encontrados:");
			for (FieldError erro : resultado.getFieldErrors()) {
				logger.info("{}", erro);
			}
			return "profissao/alterar";
		} else {
			profissaoService.alterar(profissao);
			return "redirect:/profissoes/alterar/sucesso";
		}
	}

	@GetMapping("/alterar/sucesso")
	public String mostrarMensagemAlterarSucesso(Model model) {
		model.addAttribute("mensagem", "Alteração na Profissão efetuada com sucesso.");
		return "mostrarmensagem";
	}

	@PostMapping("/abrirremover")
	public String abrirRemover(Profissao profissao) {
		return "profissao/remover";
	}

	@PostMapping("/remover")
	public String remover(Profissao profissao) {
		profissao.setStatus(Status.INATIVO);
		profissaoService.alterar(profissao);
		return "redirect:/profissoes/remover/sucesso";
	}

	@GetMapping("/remover/sucesso")
	public String mostrarMensagemRemoverSucesso(Model model) {
		model.addAttribute("mensagem", "Remoção (INATIVO) de Profissão efetuada com sucesso.");
		return "mostrarmensagem";
	}

}
