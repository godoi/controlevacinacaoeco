package web.controlevacinacao.controller;

import java.time.LocalDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import web.controlevacinacao.model.Aplicacao;
import web.controlevacinacao.model.Lote;
import web.controlevacinacao.model.Pessoa;
import web.controlevacinacao.model.Profissao;
import web.controlevacinacao.model.Status;
import web.controlevacinacao.model.Vacina;
import web.controlevacinacao.model.filter.LoteFilter;
import web.controlevacinacao.model.filter.PessoaFilter;
import web.controlevacinacao.pagination.PageWrapper;
import web.controlevacinacao.repository.LoteRepository;
import web.controlevacinacao.repository.PessoaRepository;
import web.controlevacinacao.repository.ProfissaoRepository;
import web.controlevacinacao.repository.VacinaRepository;
import web.controlevacinacao.service.AplicacaoService;
import web.controlevacinacao.service.LoteService;

@Controller
@RequestMapping("/aplicacoes")
public class AplicacaoController {

	@Autowired
	private ProfissaoRepository profissaoRepository;

	@Autowired
	private VacinaRepository vacinaRepository;

	@Autowired
	private LoteRepository loteRepository;

	@Autowired
	private PessoaRepository pessoaRepository;

	@Autowired
	private AplicacaoService aplicacaoService;
	
	@Autowired
	private LoteService loteService;

	@GetMapping("/abrircadastrar")
	public String abrirCadastrar() {
		return "aplicacao/cadastrar";
	}

	@GetMapping("/abrirescolherpessoa")
	public String abrirEscolherPessoa(Model model) {
		List<Profissao> profissoes = profissaoRepository.findByStatus(Status.ATIVO);
		model.addAttribute("profissoes", profissoes);
		return "aplicacao/pesquisarpessoa";
	}

	@GetMapping("/pesquisarpessoa")
	public String pesquisar(PessoaFilter filtro, Model model,
			@PageableDefault(size = 10) @SortDefault(sort = "codigo", direction = Sort.Direction.ASC) Pageable pageable,
			HttpServletRequest request) {

		Page<Pessoa> pagina = pessoaRepository.pesquisar(filtro, pageable);
		PageWrapper<Pessoa> paginaWrapper = new PageWrapper<>(pagina, request);
		model.addAttribute("pagina", paginaWrapper);
		return "aplicacao/mostrarpessoas";
	}

	@PostMapping("/escolherpessoa")
	public String escolherPessoa(Pessoa pessoa, HttpSession sessao) {
		Aplicacao aplicacao = (Aplicacao) sessao.getAttribute("aplicacao");
		if (aplicacao == null) {
			aplicacao = new Aplicacao();
		}
		aplicacao.setPessoa(pessoa);
		sessao.setAttribute("aplicacao", aplicacao);
		return "aplicacao/cadastrar";
	}

	@GetMapping("/abrirescolherlote")
	public String abrirEscolherLote(Model model) {
		List<Vacina> vacinas = vacinaRepository.findByStatus(Status.ATIVO);
		model.addAttribute("vacinas", vacinas);
		return "aplicacao/pesquisarlote";
	}

	@GetMapping("/pesquisarlote")
	public String pesquisar(LoteFilter filtro, Model model,
			@PageableDefault(size = 10) @SortDefault(sort = "codigo", direction = Sort.Direction.ASC) Pageable pageable,
			HttpServletRequest request) {

		Page<Lote> pagina = loteRepository.pesquisar(filtro, pageable, true);
		PageWrapper<Lote> paginaWrapper = new PageWrapper<>(pagina, request);
		model.addAttribute("pagina", paginaWrapper);
		return "aplicacao/mostrarlotes";
	}

	@PostMapping("/escolherlote")
	public String escolherLote(Lote lote, HttpSession sessao) {
		Aplicacao aplicacao = (Aplicacao) sessao.getAttribute("aplicacao");
		if (aplicacao == null) {
			aplicacao = new Aplicacao();
		}
		aplicacao.setLote(lote);
		sessao.setAttribute("aplicacao", aplicacao);
		return "aplicacao/cadastrar";
	}

	@GetMapping("/cadastrar")
	public String cadastrarAplicacao(HttpSession sessao) {
		Aplicacao aplicacao = (Aplicacao) sessao.getAttribute("aplicacao");
		if (aplicacao == null) {
			aplicacao = new Aplicacao();
		}
		aplicacao.setData(LocalDate.now());
		aplicacaoService.salvar(aplicacao);
		aplicacao.getLote().setNroDosesAtual(aplicacao.getLote().getNroDosesAtual() - 1);
        loteService.alterar(aplicacao.getLote());
        
        aplicacao.setPessoa(null);
        aplicacao.setLote(null);
        sessao.setAttribute("aplicacao", null);
        
		return "redirect:/aplicacoes/cadastrosucesso";
	}

	@GetMapping("/cadastrosucesso")
	public String mostrarCadastroSucesso(Model model) {
		model.addAttribute("mensagem", "Cadastro de aplicação efetuado com sucesso.");
		return "mostrarmensagem";
	}

}
