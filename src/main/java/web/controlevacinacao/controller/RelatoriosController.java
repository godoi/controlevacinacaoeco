package web.controlevacinacao.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import web.controlevacinacao.service.RelatorioService;

@Controller
@RequestMapping("/relatorios")
public class RelatoriosController {

	private static final Logger logger = LoggerFactory.getLogger(RelatoriosController.class);
	
	@Autowired
	private RelatorioService relatorioService;
	
	@GetMapping("/vacinas")
	public ResponseEntity<byte[]> gerarRelatorioSimplesTodasVacinas() {
		logger.debug("Gerando relatório simples de todas as vacinas");
		
		byte[] relatorio = relatorioService.gerarRelatorioSimplesTodasVacinas();
		
		logger.debug("Relatório simples de todas as vacinas gerado");
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=VacinasSimples.pdf")
				.body(relatorio);
	}
	
	@GetMapping("/vacinascompleto")
	public ResponseEntity<byte[]> gerarRelatorioCompletoTodasVacinas() {
		logger.debug("Gerando relatório completo de todas as vacinas");
		
		byte[] relatorio = relatorioService.gerarRelatorioCompletoTodasVacinas();
		
		logger.debug("Relatório Completo de todas as vacinas gerado");
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_PDF_VALUE)
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=VacinasCompleto.pdf")
				.body(relatorio);
	}
	
}