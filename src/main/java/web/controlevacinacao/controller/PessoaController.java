package web.controlevacinacao.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.data.web.SortDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import web.controlevacinacao.model.Pessoa;
import web.controlevacinacao.model.Profissao;
import web.controlevacinacao.model.Status;
import web.controlevacinacao.model.filter.PessoaFilter;
import web.controlevacinacao.pagination.PageWrapper;
import web.controlevacinacao.repository.PessoaRepository;
import web.controlevacinacao.repository.ProfissaoRepository;
import web.controlevacinacao.service.PessoaService;

@Controller
@RequestMapping("/pessoas")
public class PessoaController {

	private static final Logger logger = LoggerFactory.getLogger(PessoaController.class);
	
	@Autowired
	private ProfissaoRepository profissaoRepository;
	
	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Autowired
	private PessoaService pessoaService;

	@GetMapping("/abrirpesquisar")
	public String abrirPesquisa(Model model) {
		colocarProfissoesNoModel(model);
		return "pessoa/pesquisar";
	}
	
	@GetMapping("/pesquisar")
	public String pesquisar(PessoaFilter filtro, Model model,
			@PageableDefault(size = 10) 
    		@SortDefault(sort = "codigo", direction = Sort.Direction.ASC)
    		Pageable pageable, HttpServletRequest request) {
		
		Page<Pessoa> pagina = pessoaRepository.pesquisar(filtro, pageable);
		PageWrapper<Pessoa> paginaWrapper = new PageWrapper<>(pagina, request);
		model.addAttribute("pagina", paginaWrapper);
		return "pessoa/mostrartodas";
	}
	
	@GetMapping("/cadastrar")
	public String abrirCadastro(Pessoa pessoa, Model model) {
		colocarProfissoesNoModel(model);
		return "pessoa/cadastrar";
	}

	private void colocarProfissoesNoModel(Model model) {
		List<Profissao> profissoes = profissaoRepository.findByStatus(Status.ATIVO);
		model.addAttribute("profissoes", profissoes);
	}
	
	@PostMapping("/cadastrar")
	public String cadastrar(@Valid Pessoa pessoa, BindingResult resultado, Model model) {
		if (resultado.hasErrors()) {
			logger.info("A pessoa recebida para cadastrar não é válida.");
			logger.info("Erros encontrados:");
			for (FieldError erro : resultado.getFieldErrors()) {
				logger.info("{}", erro);
			}
			colocarProfissoesNoModel(model);
			return "pessoa/cadastrar";
		} else {
			pessoaService.salvar(pessoa);
			return "redirect:/pessoas/cadastro/sucesso";
		}
	}
	
	@GetMapping("/cadastro/sucesso")
	public String mostrarMensagemCadastroSucesso(Model model) {
		model.addAttribute("mensagem", "Cadastro de Pessoa efetuado com sucesso.");
		return "mostrarmensagem";
	}
	
	@PostMapping("/abriralterar")
	public String abrirAlterar(Pessoa pessoa, Model model) {
		colocarProfissoesNoModel(model);
		return "pessoa/alterar";
	}
	
	@PostMapping("/alterar")
	public String alterar(@Valid Pessoa pessoa, BindingResult resultado, Model model) {
		if (resultado.hasErrors()) {
			logger.info("A pessoa recebida para alterar não é válida.");
			logger.info("Erros encontrados:");
			for (FieldError erro : resultado.getFieldErrors()) {
				logger.info("{}", erro);
			}
			colocarProfissoesNoModel(model);
			return "pessoa/alterar";
		} else {
			pessoaService.alterar(pessoa);
			return "redirect:/pessoas/alterar/sucesso";
		}
	}
	
	@GetMapping("/alterar/sucesso")
	public String mostrarMensagemAlterarSucesso(Model model) {
		model.addAttribute("mensagem", "Alteração do Pessoa efetuada com sucesso.");
		return "mostrarmensagem";
	}
	
	@PostMapping("/abrirremover")
	public String abrirRemover(Pessoa pessoa) {
		return "pessoa/remover";
	}
	
	@PostMapping("/remover")
	public String remover(Pessoa pessoa) {
		pessoa.setStatus(Status.INATIVO);
		pessoaService.alterar(pessoa);
		return "redirect:/pessoas/remover/sucesso";
	}
	
	@GetMapping("/remover/sucesso")
	public String mostrarMensagemRemoverSucesso(Model model) {
		model.addAttribute("mensagem", "Remoção (INATIVO) de Pessoa efetuada com sucesso.");
		return "mostrarmensagem";
	}

}
